<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Publisher;

class PublisherController extends Controller
{
    /* Całość do poprawki */
    function list(){
        $data = [];
        foreach(Publisher::all() as $obj){
            $data[] = $obj->name;
        }
        return response()->json($data);
    }
}
