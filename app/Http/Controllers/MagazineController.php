<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;

use App\Models\Magazine;
use App\Models\Publisher;

class MagazineController extends Controller
{
    function getAll(Request $request){
        $response = Cache::get('magazines', function () {
            $value = DB::table('magazines')
            ->select('name')
            ->paginate(10);

            Cache::put('magazines', $value, now()->addMinutes(10));

            return $value;
        });
        return response()->json($response);
    }

    function searchByNameOrId(Request $request){
        if(is_numeric($request->name)){
            return response()->json(
                DB::table('magazines')
                    ->where('publisher_id', '=', $request->name)
                    ->paginate(10)
            );
        } else {
            return response()->json(
                DB::table('magazines')
                    ->whereRaw("magazines.name LIKE '%$request->name%'")
                    ->paginate(10)
            );
        }
    }
    
    function getOne(Request $request){
        return response()->json(Publisher::findOrFail($request->id)->get());
    }
}
