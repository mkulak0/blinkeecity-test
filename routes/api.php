<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\PublisherController;
use App\Http\Controllers\MagazineController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post("authorize", [AuthController::class, 'login' ]);
Route::post("register", [AuthController::class, 'register']);



Route::middleware(['auth:api'])->group( function () {
     
    Route::get("publishers/list", [PublisherController::class, 'list']);

    Route::prefix('magazines')->group(function (){
        Route::get('search/{name}', [MagazineController::class, 'searchByNameOrId']);
        Route::get('search', [MagazineController::class, 'getAll']);
        Route::get('{id}', [MagazineController::class, 'getOne']);
    });

    Route::get("logout", [AuthController::class, 'logout']);
});
